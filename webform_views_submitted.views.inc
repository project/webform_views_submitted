<?php

/**
 * Imprementation of hook_views_handlers().
 */
function webform_views_submitted_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'webform_views_submitted'),
    ),
    'handlers' => array(
      'webform_views_submitted_handler_field' => array(
        'parent' => 'views_handler_field',
        'file' => 'webform_views_submitted_handler_field.inc',
      ),
      'webform_views_submitted_handler_filter_many_to_one' => array(
        'parent' => 'views_handler_filter_many_to_one',
        'file' => 'webform_views_submitted_handler_filter_many_to_one.inc',
      ),
    ),
  );
}

/**
 * Imprementation of hook_views_submitted_data().
 */
function webform_views_submitted_views_data() {
  $res = db_query("SELECT c.cid, c.form_key, c.name, c.extra, c.type, n.title FROM {webform_component} c LEFT JOIN {node} n ON c.nid = n.nid");
  while($component = db_fetch_array($res)) {
    $data['webform_submitted_data_' . $component['form_key']]['table']['group'] = t('Webform submitted data');
    $data['webform_submitted_data_' . $component['form_key']]['table']['join'] = array(
      'webform_submissions' => array(
        'table' => 'webform_submitted_data',
        'left_field' => 'sid', 
        'field' => 'sid',
        'extra' => array(
          array(
            'field'    => 'nid',
            'value'    => 'webform_submissions.nid',
            'operator' => '=',
            'numeric'  => TRUE,
          ),
          array(
            'field'    => 'cid',
            'value'    => $component['cid'],
            'operator' => '=',
            'numeric'  => TRUE,
          ),
        ),
      ),
    );
    $data['webform_submitted_data_' . $component['form_key']][$component['form_key']] = array(
      'real field' => 'data',
      'title' => t($component['name']) . ' (' . $component['form_key'] . ')',
      'help' => t(ucwords($component['type'])) . ' - ' . t('Appears in') . ': ' . $component['title'],
      'field' => array(
        'handler' => 'webform_views_submitted_handler_field',
        'click sortable' => TRUE,
        'webform component' => $component['cid'],
      ),
      'filter' => array(
        'title' => t($component['name']) . ' (' . $component['form_key'] . ')',
        'handler' => ($component['type'] == 'select') ? 'webform_views_submitted_handler_filter_many_to_one' : 'views_handler_filter_string',
        'webform component' => $component['cid'],
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    );
  }
  return $data;
}